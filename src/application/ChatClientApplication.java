package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


/*
 * 
 * @description This is the main class for the Chat client application
 * 
 */

public class ChatClientApplication extends Application {

	@Override
	public void start(Stage primaryStage) {

		try {

			FXMLLoader fxmlLoader = new FXMLLoader();
			Parent parent = fxmlLoader.load(getClass().getResource("/chatView.fxml").openStream());
			
			Scene scene = new Scene(parent, 800, 600);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Eric's Chattservice");
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

}
