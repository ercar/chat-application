package controller;

import java.net.URL;
import java.util.ResourceBundle;
import model.ChatClientModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.*;


/*
 * 
 * @description This class controls the data flow from the gui-View to the model and also updates 
 * the gui-View whenever data changes.
 * 
 */

public class ChatController implements Initializable, Runnable {

	public TextArea chatText;
	public Button sendButton;
	public ListView<String> chatWindow = new ListView<>();
	public ObservableList<String> textlist = FXCollections.observableArrayList();
	public TextField nameTextField;

	private ChatClientModel model = new ChatClientModel();
	private Thread updateChatThread;
	private String outgoingMessage;
	private String incomingMessage;
	
	
	
	public void sendChatText() {

		outgoingMessage = nameTextField.getText() + ": "+ chatText.getText();

		model.sendMessage(outgoingMessage);
		chatText.clear();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		model.connect();

		updateChatThread = new Thread(this);
		updateChatThread.start();
		System.out.println("tr�d som uppdaterar chatten �r ig�ng!");

	}

	@Override
	public void run() {

		while (true) {

			incomingMessage = model.getMessage();

			textlist.add(incomingMessage);
			chatWindow.setItems(textlist);
			System.out.println(incomingMessage);

		}

	}

}
