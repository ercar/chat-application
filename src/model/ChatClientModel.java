package model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;



/*
 * 
 * @description This Class handles the logic for connecting, sending and receiving messages for the Chat client.
 * 
 */

public class ChatClientModel {

	private Socket socket;
	private DataInputStream in;
	private DataOutputStream out;
	private String incomingMessage;

	
	public void connect() {

		try {

			System.out.println("ansluter...");
			socket = new Socket("localhost", 7777);
			System.out.println("anslutning upprättad!");
			in = new DataInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());
			System.out.println("in- och ut-stream upprättade!");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String getMessage() {

		try {

			this.incomingMessage = in.readUTF();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return incomingMessage;
	}

	public void sendMessage(String outgoingMessage) {

		try {

			out.writeUTF(outgoingMessage);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
