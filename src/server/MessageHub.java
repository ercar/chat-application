package server;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;





/*
 * 
 * @description This Class receives messages from clients and distributes them to all other connected clients.   
 * 
 */

public class MessageHub implements Runnable {
	

	private DataOutputStream out;
	private DataInputStream in;
	private MessageHub[] messageToClient = new MessageHub[4];
	private String messageFromClient;


	public MessageHub(DataOutputStream out, DataInputStream in, MessageHub[] messageToClient) {

		this.out = out;
		this.in = in;
		this.messageToClient = messageToClient;
	}

	@Override
	public void run() {

		while (true) {

			try {

				messageFromClient = in.readUTF();
				
				for (int i = 0; i < 4; i++) {

					if (messageToClient[i] != null) {

						messageToClient[i].out.writeUTF(messageFromClient);

					}

				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

}
