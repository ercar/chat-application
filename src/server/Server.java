package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;

/*
 * 
 * @description This is the main class for the Chat server.
 * 
 */

public class Server {

	private static ServerSocket serverSocket;
	private static Socket socket;
	private static DataOutputStream out;
	private static DataInputStream in;
	private static MessageHub[] messageHubClient = new MessageHub[4]; //max number of clients.
	private static Thread thred;

	
	public static void main(String[] args) {

		try {
			System.out.println("startar servern...");
			serverSocket = new ServerSocket(7777);
			System.out.println("servern startad...");

			while (true) {
				socket = serverSocket.accept();

				for (int i = 0; i < 4; i++) {
					System.out.println("Connection from: " + socket.getInetAddress());
					out = new DataOutputStream(socket.getOutputStream());
					in = new DataInputStream(socket.getInputStream());

					if (messageHubClient[i] == null) {
						messageHubClient[i] = new MessageHub(out, in, messageHubClient);
						thred = new Thread(messageHubClient[i]);
						thred.start();
						break;

					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			try {
				socket.close();
				serverSocket.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

}
