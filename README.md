Chat Application
==========================
This software is a simple chat application made with Java and JavaFX for gui.
Also included is a server program for the clients to use.


How to use - instructions
--------------------
1. Import the project to Eclipse.
2. Run class Server.java to start the server.
3. Run class ChatClientApplication.java to start a client.




